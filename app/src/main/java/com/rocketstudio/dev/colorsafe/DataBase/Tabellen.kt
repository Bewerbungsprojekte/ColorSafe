package com.rocketstudio.dev.colorsafe.DataBase

/**
 * Created by juliusherold on 24.06.17.
 */
object ProjectTable {
    val NAME = "projects"
    val ID = "_id"
    val PROJECTNAME = "projectName"
    val COLORNAME = "colorName"
    val COLORHEX = "colorHex"
}
