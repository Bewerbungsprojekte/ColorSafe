package com.rocketstudio.dev.colorsafe.Dialogs

import android.app.Dialog
import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.view.View
import com.rocketstudio.dev.colorsafe.DataBase.ProjectDbHelper
import com.rocketstudio.dev.colorsafe.R
import kotlinx.android.synthetic.main.add_color_dialog.*
import kotlinx.android.synthetic.main.add_project_dialog.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.update

/**
 * Created by juliusherold on 03.08.17.
 */
class ColorDialog(internal var _context: Context, var database: ProjectDbHelper, var project: String, var allName: String, var allHex:String): Dialog(_context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_color_dialog)

        onClicks()
    }

    fun checkDigits(): Boolean{
        if (newColorHexTV.text.count() == 7){
            return false
        }else{
            return true
        }
    }

    private fun onClicks() {
        cancelAddColor.setOnClickListener {
            this.dismiss()
        }

        hexChar()

        saveAddColor.setOnClickListener {
            var newHex = newColorHexTV.text.toString() + ";"
            var newName = "#" + newColorNameET.text.toString() + ";"
            if (newColorNameET.text.toString().isEmpty()){

            }else{
                try {
                    database.use {
                        update("projects", "colorName" to allName + newName, "colorHex" to allHex + newHex)
                                .whereSimple("projectName = ?", project)
                                .exec()
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }
                this.dismiss()
            }

        }




    }


    fun hexChar(){
        hex1.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("1")
            }
        }
        hex2.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("2")
            }
        }
        hex3.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("3")
            }
        }
        hex4.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("4")
            }
        }
        hex5.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("5")
            }
        }
        hex6.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("6")
            }
        }
        hex7.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("7")
            }
        }
        hex8.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("8")
            }
        }
        hex9.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("9")
            }
        }
        hex0.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("0")
            }
        }
        hexA.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("A")
            }
        }
        hexB.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("B")
            }
        }
        hexC.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("C")
            }
        }
        hexD.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("D")
            }
        }
        hexE.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("E")
            }
        }
        hexF.setOnClickListener {
            if (checkDigits()){
                newColorHexTV.append("F")
            }
        }
        hexDelete.setOnClickListener {
            if (newColorHexTV.text.count() > 1){
               newColorHexTV.text = newColorHexTV.text.substring(0,newColorHexTV.length()-1)
            }
        }
        hexContinue.setOnClickListener {
            if (newColorHexTV.text.count() == 7){
                hexFieldLL.visibility = View.GONE
                nameFieldLL.visibility = View.VISIBLE
            }
        }
    }

}
