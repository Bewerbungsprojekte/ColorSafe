package com.rocketstudio.dev.colorsafe

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.rocketstudio.dev.colorsafe.DataBase.ProjectDbHelper
import com.rocketstudio.dev.colorsafe.DataBase.ProjectTable
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.db.dropTable
import android.content.Intent
import android.content.ActivityNotFoundException
import android.net.Uri


/**
 * Created by juliusherold on 07.08.17.
 */
class SettingsActivity : AppCompatActivity() {

    var database = ProjectDbHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        initView()

    }

    private fun initView() {

        var code = 0x1F495
        var endText = "Made with ${getEmoji(code)} and Kotlin. \n By JH-Software"


        love.text = endText

        onClicks()

    }

    private fun onClicks() {
        deleteAll.setOnClickListener {
            try {
                database.getInstance(applicationContext).use {
                    dropTable("projects", true)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        rateApp.setOnClickListener{
            val uri = Uri.parse("market://details?id=" + applicationContext.packageName)
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)

            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
            try {
                startActivity(goToMarket)
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + applicationContext.packageName)))
            }

        }
    }

    fun getEmoji(test: Int): String {
        var string = String(Character.toChars(test))
        return string
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)


        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

}