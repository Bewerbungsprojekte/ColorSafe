package com.rocketstudio.dev.colorsafe.Controlls

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.rocketstudio.dev.colorsafe.ProjectActivity
import com.rocketstudio.dev.colorsafe.R
import kotlinx.android.synthetic.main.color_control.view.*

/**
 * Created by juliusherold on 28.07.17.
 */
class ColorControll : LinearLayout {

    var _context: Context
    var _activity: ProjectActivity? = null
    var _colorName = ""
    var _colorHex = ""

    constructor(context: Context) : super(context) {
        _context = context
        initControl(context)

    }

    constructor(context: Context, activity: ProjectActivity, colorHex: String, colorName: String) : super(context) {
        _context = context
        _colorName = colorName
        _colorHex = colorHex
        initControl(context)
        _activity = activity

        onClicks()
    }


    private fun initControl(context: Context) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.color_control, this)

        try {
            colorView.setBackgroundColor(Color.parseColor("#$_colorHex"))
        } catch (e: Exception) {
            e.printStackTrace()
        }


        colorControlName.text = _colorName

    }

    private fun onClicks() {

    }


}