package com.rocketstudio.dev.colorsafe.Controlls

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.Toast
import com.rocketstudio.dev.colorsafe.ClickListener
import com.rocketstudio.dev.colorsafe.DataBase.ProjectDbHelper
import com.rocketstudio.dev.colorsafe.MainActivity
import com.rocketstudio.dev.colorsafe.R
import kotlinx.android.synthetic.main.color_control.view.*
import kotlinx.android.synthetic.main.project_control.view.*
import org.jetbrains.anko.db.StringParser
import org.jetbrains.anko.db.select

/**
 * Created by juliusherold on 18.06.17.
 */
class ProjectCircle : LinearLayout {

    var colorName: String? = null
    var colorHex: String? = null
    var _context: Context
    var _activity: MainActivity? = null

    var listener: ClickListener? = null

    constructor(context: Context) : super(context) {
        _context = context
        initControl(context)

    }

    constructor(context: Context, activity: MainActivity) : super(context) {
        _context = context
        initControl(context)
        _activity = activity

        onClicks()
    }

    constructor(context: Context, attributeSet: AttributeSet, activity: MainActivity) : super(context) {
        _context = context
        initControl(context)

        onClicks()
    }

    fun initControl(context: Context) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.project_control, this)
    }

    fun onClicks() {


        this.setOnClickListener {

            _activity?.loadNew(colorName!!)

        }

        this.setOnLongClickListener {

            Toast.makeText(_context, "Long Klick", Toast.LENGTH_SHORT).show()

            true

        }

    }


    fun createProject(database: ProjectDbHelper, position: String) {
        database.use {

            var projectName = select("projects").where("rowid = {id}",
                    "id" to position).column("projectName").parseList(StringParser)

            colorName = projectName.toString().removeSurrounding("[", "]")

            rootView.colorName.setText(colorName)
            rootView.colorHex.setText("")

        }
    }


}