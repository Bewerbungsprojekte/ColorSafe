package com.rocketstudio.dev.colorsafe.Dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.rocketstudio.dev.colorsafe.DataBase.ProjectDbHelper
import com.rocketstudio.dev.colorsafe.DataBase.ProjectTable
import com.rocketstudio.dev.colorsafe.R
import kotlinx.android.synthetic.main.add_project_dialog.*
import org.jetbrains.anko.db.insert

/**
 * Created by juliusherold on 27.06.17.
 */

class ProjectDialog(internal var _context:Context, var database: ProjectDbHelper):Dialog(_context){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_project_dialog)

        onClicks()
    }

    private fun onClicks() {
        cancelAddProject.setOnClickListener {
            this.dismiss()
        }

        saveAddProject.setOnClickListener {
            var project = newProjectET.text.toString()

            if (project.isEmpty()){

            }else{
                database.use {
                    insert(ProjectTable.NAME,
                            ProjectTable.PROJECTNAME to project)
                }
                this.dismiss()
            }

        }
    }

}