package com.rocketstudio.dev.colorsafe

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Window
import com.rocketstudio.dev.colorsafe.Controlls.ColorControll
import com.rocketstudio.dev.colorsafe.DataBase.ProjectDbHelper
import com.rocketstudio.dev.colorsafe.Dialogs.ColorDialog
import com.rocketstudio.dev.colorsafe.Dialogs.ProjectDialog

import kotlinx.android.synthetic.main.activity_project.*
import kotlinx.android.synthetic.main.content_project.*
import org.jetbrains.anko.db.StringParser
import org.jetbrains.anko.db.select

class ProjectActivity : AppCompatActivity() {

    var database = ProjectDbHelper
    var projectname = ""
    var colorsHex = ""
    var colorsName = ""
    var allColorName = ""
    var allColorHex = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project)
        setSupportActionBar(toolbar)
3
        initView()

        fab.setOnClickListener { view ->

            val newColor = ColorDialog(this, database.getInstance(applicationContext), projectname, allColorName, allColorHex)
            newColor.setCancelable(true)
            newColor.requestWindowFeature(Window.FEATURE_NO_TITLE)
            newColor.show()

            newColor.setOnDismissListener{
                loadData()
            }


        }
    }

    private fun initView() {
        var bundle = intent.extras
        projectname = bundle.getString("projectName")
        nameProject.setText(projectname)

        loadData()
    }

    private fun loadData() {

        try {
            projectContentPanel.removeAllViews()
            database.getInstance(this).use {

                var dbNames = select("projects").where("projectName = {pName}",
                        "pName" to projectname).column("colorName").parseList(StringParser)

                var dbHex = select("projects").where("projectName = {pName}",
                        "pName" to projectname).column("colorHex").parseList(StringParser)
                allColorName = dbNames.toString().removeSurrounding("[","]")
                allColorHex = dbHex.toString().removeSurrounding("[","]")
            }

            var counter = 0
            var currentColor = ""
            var currentColorName = ""

            if (allColorName.equals("null")){
                allColorName = ""
            }

            if (allColorHex.equals("null")){
                allColorHex = ""
            }

            colorsHex = allColorHex
            colorsName = allColorName

            while (colorsHex.contains("#")){
                counter++
                currentColor = colorsHex.substring(1,colorsHex.indexOf(";"))
                currentColorName = colorsName.substring(1,colorsName.indexOf(";"))

                var color = ColorControll(applicationContext, this, currentColor,currentColorName)

                colorsHex = colorsHex.substring(colorsHex.indexOf(";")+1)
                colorsName = colorsName.substring(colorsName.indexOf(";")+1)

                projectContentPanel.addView(color)
            }


        }catch (e: Exception){

            nameProject.setText("Leer")

        }








    }

}
