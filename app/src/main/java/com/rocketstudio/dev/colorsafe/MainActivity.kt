package com.rocketstudio.dev.colorsafe

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import com.facebook.stetho.Stetho
import com.rocketstudio.dev.colorsafe.Controlls.ProjectCircle
import com.rocketstudio.dev.colorsafe.DataBase.ProjectDbHelper
import com.rocketstudio.dev.colorsafe.DataBase.ProjectTable
import com.rocketstudio.dev.colorsafe.Dialogs.ProjectDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.db.*
import org.jetbrains.anko.startActivity


class MainActivity : AppCompatActivity(), ClickListener {


    var datenSätze = 0
    var datenDurchgänge = 0
    var gesammteDurchgänge = 0
    var database = ProjectDbHelper
    var test = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Stetho.initializeWithDefaults(this)

        fab.setOnClickListener { view ->


            val newProject = ProjectDialog(this, database.getInstance(applicationContext))
            newProject.setCancelable(false)
            newProject.requestWindowFeature(Window.FEATURE_NO_TITLE)
            newProject.show()

            newProject.setOnDismissListener {
                try {
                    countDbEntries()
                    loadProjects()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
        try {
            countDbEntries()
            loadProjects()
        } catch (e: Exception) {
            database.getInstance(applicationContext).use {
                createTable(ProjectTable.NAME, true,
                        ProjectTable.PROJECTNAME to TEXT,
                        ProjectTable.COLORNAME to TEXT,
                        ProjectTable.COLORHEX to TEXT)
            }
            e.printStackTrace()
        }

    }

    private fun countDbEntries() {
        database.getInstance(applicationContext).use {
            datenSätze = select("projects").exec {
                this.count
            }
        }
    }

    private fun showData() {
        database.getInstance(applicationContext).use {

            try {
                var result = select("projects").where("rowid = {id}",
                        "id" to 1).column("colorHex").parseList(StringParser)

                Toast.makeText(applicationContext, result.toString(), Toast.LENGTH_LONG).show()

            } catch(e: Exception) {
                Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun dataFromDatabase() {
        try {
            database.getInstance(applicationContext).use {
                insert("projects",
                        "projectName" to "MillionDollar",
                        "colorName" to "Grün",
                        "colorHex" to "1000000")
            }
        } catch(e: Exception) {
            Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            countDbEntries()
            loadProjects()
        } catch (e: Exception) {
            database.getInstance(applicationContext).use {
                createTable(ProjectTable.NAME, true,
                        ProjectTable.PROJECTNAME to TEXT,
                        ProjectTable.COLORNAME to TEXT,
                        ProjectTable.COLORHEX to TEXT)
            }        }
    }

    private fun dataFromRow(currentEntry: String) {
        database.getInstance(applicationContext).use {
            select("project").whereSimple("_id", currentEntry).exec {

            }
        }
    }

    private fun loadProjects() {
        contentPanel.removeAllViews()
        datenDurchgänge = 0
        gesammteDurchgänge = 0
        while (datenDurchgänge < datenSätze) {
            addRow()
        }
    }

    private fun loadFirstOpening() {
        var welcomeText = TextView(applicationContext)
        welcomeText.setText("Wilkommen bei ColorSafe! Erstelle dein erstes Projekt")
        contentPanel.addView(welcomeText)
    }

    private fun addRow() {
        var i = 0
        var linearLayout = LinearLayout(applicationContext)
        contentPanel.addView(linearLayout)
        while (i < 3) {
            if (gesammteDurchgänge != datenSätze) {
                var circle = ProjectCircle(applicationContext, this)
                gesammteDurchgänge++
                circle.createProject(database.getInstance(applicationContext), gesammteDurchgänge.toString())
                linearLayout.addView(circle)
            }
            i++
        }
        datenDurchgänge++
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)



        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> startSettings()

        }
        return true
    }

    fun startSettings() {
        try {
            val intentSettings = Intent(this, SettingsActivity::class.java)
            startActivity(intentSettings)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    public fun loadNew(projectName: String) {
        val intent = Intent(this, ProjectActivity::class.java)
        intent.putExtra("projectName", projectName)
        startActivity(intent)
    }

    override fun onClicked() {
        val intent = Intent(this, ProjectActivity::class.java)
        startActivity(intent)
    }
}
