package com.rocketstudio.dev.colorsafe

/**
 * Created by juliusherold on 26.07.17.
 */
interface ClickListener {
    fun onClicked()
}