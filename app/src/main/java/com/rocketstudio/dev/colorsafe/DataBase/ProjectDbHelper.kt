package com.rocketstudio.dev.colorsafe.DataBase

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.rocketstudio.dev.colorsafe.*
import org.jetbrains.anko.db.*

/**
 * Created by juliusherold on 24.06.17.
 */
class ProjectDbHelper(context: Context) : ManagedSQLiteOpenHelper(context,"projects", null, 1){

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(ProjectTable.NAME, true)
        onCreate(db)
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(ProjectTable.NAME, true,
                ProjectTable.PROJECTNAME to TEXT,
                ProjectTable.COLORNAME to TEXT,
                ProjectTable.COLORHEX to TEXT)
    }

    companion object {
        private var instance: ProjectDbHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): ProjectDbHelper {
            if (instance == null) {
                instance = ProjectDbHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

}